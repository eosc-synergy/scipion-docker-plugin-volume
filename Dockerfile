
FROM nvidia/cudagl:10.1-runtime-ubuntu18.04

RUN apt update
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata

RUN apt install --no-install-recommends -y \
    gcc-5 g++ cmake openjdk-8-jdk libxft-dev libssl-dev libxext-dev\
    libxml2-dev libreadline7 libquadmath0 libxslt1-dev libopenmpi-dev openmpi-bin\
    libxss-dev libgsl0-dev libx11-dev gfortran libfreetype6-dev scons libfftw3-dev libopencv-dev curl git

RUN apt install --no-install-recommends -y \
    rsync libssl1.0.0 wget bison flex

RUN rm -rf /var/lib/apt/lists/*

RUN curl -fsSL -o /tmp/scipion_latest_linux64_Ubuntu.tgz http://scipion.i2pc.es/startdownload/?bundleId=4
RUN cd /tmp/ && \
    tar -xzf scipion_latest_linux64_Ubuntu.tgz && \
    mv /tmp/scipion /opt/ && \
    rm /tmp/scipion_latest_linux64_Ubuntu.tgz

# Create scipionuser
RUN groupadd --gid 1042 scipionuser && \
    useradd --uid 1042 --create-home --home-dir /home/scipionuser -s /bin/bash -g scipionuser scipionuser

RUN usermod -aG sudo scipionuser

# Prepare home and Scipion for scipionuser
RUN chown -R scipionuser:scipionuser /home/scipionuser && \
    chown -R scipionuser:scipionuser /opt/scipion

USER scipionuser
#######################

# Install scipion
RUN echo "" | /opt/scipion/scipion config

RUN sed -i 's/MPI_LIBDIR\s*=.*/MPI_LIBDIR = \/usr\/lib\/x86_64-linux-gnu\/openmpi\/lib/' /opt/scipion/config/scipion.conf && \
    sed -i 's/MPI_INCLUDE\s*=.*/MPI_INCLUDE = \/usr\/lib\/x86_64-linux-gnu\/openmpi\/include/' /opt/scipion/config/scipion.conf && \
    sed -i 's/CUDA\s*=.*/CUDA = True/' /opt/scipion/config/scipion.conf && \
    sed -i 's/CUDA_LIB\s*=.*/CUDA_LIB = \/usr\/local\/cuda\/lib64/' /opt/scipion/config/scipion.conf && \
    sed -i 's/CUDA_BIN\s*=.*/CUDA_BIN = \/usr\/local\/cuda\/bin/' /opt/scipion/config/scipion.conf && \
    echo "RELION_CUDA_LIB = /usr/local/cuda/lib64" >>  /opt/scipion/config/scipion.conf && \
    echo "RELION_CUDA_BIN = /usr/local/cuda/bin" >>  /opt/scipion/config/scipion.conf && \
    echo "MOTIONCOR2_BIN = MotionCor2_1.3.0-Cuda101" >>  /opt/scipion/config/scipion.conf && \
    echo "GCTF = Gctf_v1.18_sm30-75_cu10.1" >>  /opt/scipion/config/scipion.conf && \
    echo "GAUTOMATCH = Gautomatch_v0.56_sm30-75_cu10.1" >>  /opt/scipion/config/scipion.conf && \
    sed -i 's/NVCC_INCLUDE\s*=.*/NVCC_INCLUDE = \/usr\/local\/cuda\/include/' /opt/scipion/config/scipion.conf

COPY plugin-list-pl.txt /
COPY plugin-list-bin.txt /


RUN export CORE_COUNT=$(nproc) && \
    /opt/scipion/scipion install -j $CORE_COUNT && \
    \
    for pl in $(cat /plugin-list-pl.txt); do /opt/scipion/scipion installp -p $pl -j $CORE_COUNT; done && \
    \
    for bin in $(cat /plugin-list-bin.txt); do /opt/scipion/scipion installb $bin -j $CORE_COUNT; done

RUN cp /plugin-list-pl.txt /opt/scipion/software/em/ && \
    cp /plugin-list-bin.txt /opt/scipion/software/em/

USER root
#######################

RUN cd /opt/scipion/software/ && for file in $(ls -A); do if [ "$file" != "em" ]; then rm -rf $file; fi; done
RUN cd /opt/scipion/ && for file in $(ls -A); do if [ "$file" != "software" ]; then rm -rf $file; fi; done
RUN cd /opt/ && for file in $(ls -A); do if [ "$file" != "scipion" ]; then rm -rf $file; fi; done

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

RUN ls -la /opt
RUN ls -la /opt/scipion
RUN ls -la /opt/scipion/software/em

ENTRYPOINT ["/docker-entrypoint.sh"]
